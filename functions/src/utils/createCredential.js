/**
 * Creates an oath2 client credential to request auth api.
 * @param  {Object} client - Client credentials
 * @param  {Object} auth - Auth token paths and authorize path
 * @return {Promise} Resolve the credential cwith requested parameters
 */
export function oauth2Client(client, auth) {
  const credentials = {
    client: {
      id: client.id,
      secret: client.secret
    },
    auth: {
      tokenHost: auth.tokenHost,
      tokenPath: auth.tokenPath,
      authorizePath: auth.authorizePath
    }
  }
  return require('simple-oauth2').create(credentials)
}
