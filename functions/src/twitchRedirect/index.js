import * as functions from 'firebase-functions'
import redirectTwitch from './redirectTwitch'

/**
 * @name twitchRedirect
 * Call a Google API with a Service Account
 * @type {functions.CloudFunction}
 */
export default functions.https.onRequest(redirectTwitch)
