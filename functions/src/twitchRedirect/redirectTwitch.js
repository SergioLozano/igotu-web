import * as functions from 'firebase-functions'
import * as cookieParser from 'cookie-parser'
import crypto from 'crypto'
import { oauth2Client } from '../utils/createCredential'
import {
  TWITCH_AUTH_TOKEN_HOST,
  TWITCH_AUTH_TOKEN_PATH,
  TWITCH_AUTH_AUTHORIZE,
  TWITCH_REDIRECT_URI,
  TWITCH_OAUTH_SCOPES
} from './constants'

async function twitchCredentials() {
  const client = {
    id: functions.config().twitch.client_id,
    secret: functions.config().twitch.client_secret
  }
  const auth = {
    tokenHost: TWITCH_AUTH_TOKEN_HOST,
    tokenPath: TWITCH_AUTH_TOKEN_PATH,
    authorizePath: TWITCH_AUTH_AUTHORIZE
  }
  return oauth2Client(client, auth)
}

export default async function redirectTwitch(req, res) {
  const oauth2 = twitchCredentials()
  cookieParser()(req, res, () => {
    const state = req.cookies.state || crypto.randomBytes(20).toString('hex')
    console.log(`Setting verification state:`, state)
    res.cookie('state', state.toString(), {
      maxAge: 3600000,
      secure: true,
      httpOnly: true
    })
    const redirectUri = oauth2.authorizationCode.authorizeURL({
      redirect_uri: TWITCH_REDIRECT_URI,
      scope: TWITCH_OAUTH_SCOPES,
      state: state
    })
    console.log(`Redirecting to:`, redirectUri)
    res.redirect(redirectUri)
  })
}
