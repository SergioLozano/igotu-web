export const TWITCH_AUTH_TOKEN_HOST = 'https://id.twitch.tv'
export const TWITCH_AUTH_TOKEN_PATH = '/oauth2/token'
export const TWITCH_AUTH_AUTHORIZE = '/oauth2/token'
