export const PRIZES_PATH = '/prizes'
export const ACCOUNT_PATH = '/account'
export const LOGIN_PATH = '/login'
export const SIGNUP_PATH = '/signup'
export const ACCOUNT_FORM_NAME = 'account'
export const LOGIN_FORM_NAME = 'login'
export const SIGNUP_FORM_NAME = 'signup'
export const PERMISSIONS_PATH = 'permissions'

export const paths = {
  prizes: PRIZES_PATH,
  account: ACCOUNT_PATH,
  login: LOGIN_PATH,
  signup: SIGNUP_PATH,
  projectPermissions: PERMISSIONS_PATH
}

export default paths
