import PropTypes from 'prop-types'
import { compose } from 'redux'
import { reduxForm } from 'redux-form'
import { withHandlers, setPropTypes } from 'recompose'
import { LOGIN_FORM_NAME } from 'constants/formNames'
import withFirebase from 'react-redux-firebase/lib/withFirebase'

export default compose(
  // add props.firebase
  withFirebase,
  // set proptypes used in HOCs
  setPropTypes({
    onSubmit: PropTypes.func.isRequired // used by reduxForm
  }),
  // set proptypes used in HOCs
  setPropTypes({
    onSubmit: PropTypes.func.isRequired // used by reduxForm
  }),
  reduxForm({
    form: LOGIN_FORM_NAME
    // Clear the form for future use (creating another project)
    //onSubmitSuccess: (result, dispatch, props) => props.reset()
  }),
  withHandlers({
    closeAndReset: props => () => {
      props.reset()
      props.onRequestClose && props.onRequestClose()
    }
  })
)
