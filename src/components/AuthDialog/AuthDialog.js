import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import TextField from 'components/FormTextField'
//import { required, validateEmail } from 'utils/form'
//import { triggerAnalyticsEvent } from 'utils/analytics'
//import { PRIZES_PATH } from 'constants/paths'
import {
  Modal,
  Button,
  Card,
  CardHeader,
  CardBody,
  Form,
  Row,
  Col
} from 'reactstrap'

function AuthDialog({
  handleSubmit,
  submitting,
  pristine,
  firebase,
  history,
  open,
  onRequestClose
}) {
  // const [isLoading, changeLoadingState] = useState(false)
  // function loginWithEmail(values) {
  //   triggerAnalyticsEvent('emailLogin', { category: 'Auth', action: 'Login' })
  //   changeLoadingState(true)
  //   return firebase
  //     .login({ email: 'test@test.com', password: 'testest1' })
  //     .then(() => {
  //       history.push(PRIZES_PATH)
  //     })
  //     .catch(err => showError(err.message))
  // }
  return (
    <Modal
      className="modal-dialog-centered"
      isOpen={open}
      toggle={onRequestClose}>
      <Row className="justify-content-center">
        <Col>
          <Card className="bg-secondary shadow border-0">
            <CardHeader className="bg-white pb-5">
              <div className="text-muted text-center mb-3">
                <small>Sign in with</small>
              </div>
              <div className="btn-wrapper text-center">
                <Button
                  className="btn-twitch btn-icon ml-1"
                  color="#8A2BE2"
                  onClick={e => e.preventDefault()}>
                  <span className="btn-inner--icon mr-1">
                    <img
                      alt="..."
                      src={require('assets/img/icons/common/twitch.svg')}
                    />
                  </span>
                  <span className="btn-inner--text">LogIn with Twitch</span>
                </Button>
              </div>
            </CardHeader>
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Or sign in with email</small>
              </div>
              <Form role="form" onSubmit={handleSubmit}>
                <Field
                  name="email"
                  type="email"
                  placeHolder="Email"
                  icon="ni ni-email-83"
                  component={TextField}
                  //validate={[required, validateEmail]}
                />
                <Field
                  name="password"
                  placeholder="Password"
                  type="password"
                  autoComplete="off"
                  icon="ni ni-lock-circle-open"
                  component={TextField}
                  //validate={[required]}
                />
                <div className="text-center">
                  <Button
                    className="my-4"
                    color="primary"
                    type="submit"
                    disabled={submitting}>
                    {submitting ? 'Siging In' : 'Sign In'}
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Modal>
  )
}

AuthDialog.propTypes = {
  handleSubmit: PropTypes.func.isRequired, // from enhancer (reduxForm)
  pristine: PropTypes.bool.isRequired, // from enhancer (reduxForm)
  submitting: PropTypes.bool.isRequired, // from enhancer (reduxForm)
  open: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired
}

export default AuthDialog
