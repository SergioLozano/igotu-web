import AuthDialog from './AuthDialog'
import enhance from './AuthDialog.enhancer'

export default enhance(AuthDialog)
