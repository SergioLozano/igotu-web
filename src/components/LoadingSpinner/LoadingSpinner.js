import React from 'react'
import PropTypes from 'prop-types'
import { Spinner } from 'reactstrap'

function LoadingSpinner({ size }) {
  return (
    <div>
      <div>
        <Spinner color="primary" style={{ width: size || 80 }} />
      </div>
    </div>
  )
}

LoadingSpinner.propTypes = {
  size: PropTypes.number
}

export default LoadingSpinner
