import React from 'react'
import PropTypes from 'prop-types'
import {
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup
} from 'reactstrap'

function FormTextField({
  label,
  placeHolder,
  icon,
  type,
  input,
  autoComplete,
  meta: { touched, invalid, error },
  ...custom
}) {
  return (
    <FormGroup className="mb-3">
      <InputGroup className="input-group-alternative">
        <InputGroupAddon addonType="prepend">
          <InputGroupText>
            <i className={icon} />
          </InputGroupText>
        </InputGroupAddon>
        <Input
          placeholder={placeHolder}
          type={type}
          autoComplete={autoComplete}
          {...input}
          {...custom}
        />
      </InputGroup>
    </FormGroup>
  )
}

FormTextField.propTypes = {
  formTextField: PropTypes.object
}

export default FormTextField
