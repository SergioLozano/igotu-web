import React from 'react'
import PropTypes from 'prop-types'
//import classNames from 'classnames'
import { connect } from 'react-redux'
import * as actions from '../actions'

function Notifications({
  className,
  allIds,
  byId,
  variant = 'info',
  dismissNotification
}) {
  // Only render if notifications exist
  if (!allIds || !Object.keys(allIds).length) {
    return null
  }

  return (
    <div>
      {allIds.map(id => {
        //const Icon = variantIcon[byId[id].type] || variantIcon[variant]
        // function dismissNotificationById() {
        //   return dismissNotification(id)
        // }
        return (
          <></>
          // <Snackbar
          //   key={id}
          //   anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          //   open
          //   className={classes.snackbar}>
          //   <SnackbarContent
          //     className={classNames(
          //       classes[byId[id].type] || classes[variant],
          //       className
          //     )}
          //     aria-describedby="client-snackbar"
          //     message={
          //       <span
          //         id="client-snackbar"
          //         data-test="notification-message"
          //         className={classes.message}>
          //         <Icon className={classes.icon} />
          //         {byId[id].message}
          //       </span>
          //     }
          //     action={[
          //       <IconButton
          //         key="close"
          //         aria-label="Close"
          //         color="inherit"
          //         className={classes.close}
          //         onClick={dismissNotificationById}>
          //         <CloseIcon className={classes.icon} />
          //       </IconButton>
          //     ]}
          //   />
          // </Snackbar>
        )
      })}
    </div>
  )
}

Notifications.propTypes = {
  allIds: PropTypes.array.isRequired,
  byId: PropTypes.object.isRequired,
  variant: PropTypes.string,
  className: PropTypes.string,
  dismissNotification: PropTypes.func.isRequired
}

export default connect(
  ({ notifications: { allIds, byId } }) => ({ allIds, byId }),
  actions
)(Notifications)
