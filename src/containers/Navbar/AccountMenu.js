import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { ACCOUNT_PATH } from 'constants/paths'
import enhancer from './Navbar.enhancer'
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown
} from 'reactstrap'

function AccountMenu({ firebase, history }) {
  function handleLogout() {
    return firebase.logout().then(() => {
      history.push('/')
    })
  }
  function goToAccount() {
    history.push(ACCOUNT_PATH)
  }

  return (
    <Fragment>
      <UncontrolledDropdown group>
        <DropdownToggle caret color="primary">
          Account
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem href="#pablo" onClick={goToAccount}>
            Account
          </DropdownItem>
          <DropdownItem href="#pablo" onClick={handleLogout}>
            Sign Out
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    </Fragment>
  )
}

AccountMenu.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }),
  firebase: PropTypes.shape({
    logout: PropTypes.func.isRequired
  })
}

export default enhancer(AccountMenu)
