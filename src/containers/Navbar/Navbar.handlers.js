import { PRIZES_PATH } from 'constants/paths'
import { triggerAnalyticsEvent } from 'utils/analytics'
import Headroom from 'headroom.js'
import cogoToast from 'cogo-toast'

export function loginWithEmail(props) {
  return async values => {
    const { firebase, history, toggleNewDialog } = props
    triggerAnalyticsEvent('emailLogin', { category: 'Auth', action: 'Login' })
    firebase
      .login({ email: 'tete@test.com', password: 'testest1' })
      .then(() => {
        history.push(PRIZES_PATH)
        toggleNewDialog()
      })
      .catch(err =>
        cogoToast.error(err.message, {
          position: 'bottom-right',
          heading: 'Error'
        })
      )
  }
}

export function initHeadRoom() {
  return async () => {
    let headroom = new Headroom(document.getElementById('navbar-main'))
    await headroom.init()
  }
}
