import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStateHandlers, setDisplayName } from 'recompose'
import { withRouter } from 'react-router-dom'
//import firestoreConnect from 'react-redux-firebase/lib/firestoreConnect'
import { spinnerWhileLoading } from 'utils/components'
import { UserIsAuthenticated } from 'utils/router'
//import * as handlers from './PrizesPage.handlers'

export default compose(
  // Set component display name (more clear in dev/error tools)
  setDisplayName('EnhancedPrizesPage'),
  // redirect to /login if user is not logged in
  UserIsAuthenticated,
  // Map auth uid from state to props
  connect(({ firebase: { auth: { uid } } }) => ({ uid })),
  // Wait for uid to exist before going further
  spinnerWhileLoading(['uid']),
  // Create listeners based on current users UID
  //   firestoreConnect(({ uid }) => [
  //     // Listener for projects the current user created
  //     {
  //       collection: 'projects',
  //       where: ['createdBy', '==', uid]
  //     },
  //     // Listener for projects current user collaborates on
  //     {
  //       collection: 'projects',
  //       where: [`collaborators.${uid}`, '==', true],
  //       storeAs: 'collabProjects'
  //     }
  //   ]),
  // Map projects from state to props
  //   connect((state, props) => ({
  //     projects: getAllCurrentUsersProjects(state, props)
  //   })),
  // Show loading spinner while projects and collabProjects are loading
  //spinnerWhileLoading(['projects']),
  // Add props.router
  withRouter,
  // Add state and state handlers as props
  withStateHandlers(
    // Setup initial state
    ({ initialDialogOpen = false }) => ({
      newDialogOpen: initialDialogOpen
    }),
    // Add state handlers as props
    {
      toggleDialog: ({ newDialogOpen }) => () => ({
        newDialogOpen: !newDialogOpen
      })
    }
  )
  // Add handlers as props
  //withHandlers(handlers)
)
