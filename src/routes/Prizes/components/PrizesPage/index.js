import PrizesPage from './PrizesPage'
import enhance from './PrizesPage.enhancer'

export default enhance(PrizesPage)
