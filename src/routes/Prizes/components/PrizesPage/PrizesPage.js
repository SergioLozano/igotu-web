import React from 'react'
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button
} from 'reactstrap'

function ProjectsPage({
  projects,
  collabProjects,
  auth,
  newDialogOpen,
  toggleDialog,
  deleteProject,
  addProject,
  match,
  goToProject
}) {
  return (
    <div className="position-relative">
      <Card>
        <CardImg
          top
          width="100%"
          src={require('assets/img/theme/img-1-1200x1000.jpg')}
          alt="Card image cap"
        />
        <CardBody>
          <CardTitle>Card title</CardTitle>
          <CardSubtitle>Card subtitle</CardSubtitle>
          <CardText>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
    </div>
  )
}

// ProjectsPage.propTypes = {
//   match: PropTypes.object.isRequired, // from enhancer (withRouter)
//   auth: PropTypes.object, // from enhancer (connect + firebaseConnect - firebase)
//   projects: PropTypes.array, // from enhancer (connect + firebaseConnect - firebase)
//   newDialogOpen: PropTypes.bool, // from enhancer (withStateHandlers)
//   toggleDialog: PropTypes.func.isRequired, // from enhancer (withStateHandlers)
//   deleteProject: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
//   collabProjects: PropTypes.object, // from enhancer (withHandlers - firebase)
//   addProject: PropTypes.func.isRequired, // from enhancer (withHandlers - firebase)
//   goToProject: PropTypes.func.isRequired // from enhancer (withHandlers - router)
// }

export default ProjectsPage
