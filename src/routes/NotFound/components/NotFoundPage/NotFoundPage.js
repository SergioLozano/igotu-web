import React from 'react'
import { Container } from 'reactstrap'

function NotFoundPage({ history }) {
  return (
    <div className="position-relative">
      {/* shape Hero */}
      <div className="shape shape-style-1 shape-default"></div>
      <Container className="py-lg-md d-flex">
        <p className="h1">404. Page Not Found</p>
      </Container>
    </div>
  )
}

export default NotFoundPage
