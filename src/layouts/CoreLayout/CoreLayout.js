import React from 'react'
import PropTypes from 'prop-types'
import NavbarContainer from 'containers/Navbar'
import VersionChangeReloader from 'components/VersionChangeReloader'

function CoreLayout({ children }) {
  return (
    <div>
      <NavbarContainer />
      <div>{children}</div>
      <VersionChangeReloader />
    </div>
  )
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired
}

export default CoreLayout
